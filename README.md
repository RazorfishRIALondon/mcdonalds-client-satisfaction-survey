### Admin

## Google App Engine Account

razorfish.ria.mcdonalds@gmail.com

## Domains

We have 3 domains:

* Live: https://razorfish-client-survey.appspot.com
* Live: https://razorfish-mcdonalds-survey.appspot.com (McDonald's specific Clients)
* QA: https://razorfish-client-survey-qa.appspot.com

To set the domain for your deployment uncomment the correct domain in the app.yaml file at the root of the project

## View Admin

1. go to DOMAIN/admin

You can then sleect each Section to see results in Pie Chart format


## Add Clients

1. Log into DOMAIN/admin/clients
* You will see a list of existing Clients
2. Fill in the Client details and click "Add Client"
* Name
* Role
* Email
* Account
* Owner
3. The Client list will be updated, and the Client will have a unique Survey URL
* Provide this to the Razorfish Client Services rep, e.g. Matt Warren
* Perhaps in a spreadsheet with Name, Email and Survey URL

## Generate Reports

# Generate A CSV File Report

1. Log into GMail with razorfish.ria.mcdonalds@gmail.com (See Carl Hendrickse for password)
2. Go to DOMAIN/admin/report?brand=BRAND
* Where DOMAIN is one of the 2 domains above and BRAND is 'razorfish', 'mcdonalds', etc
* If a successful report has been generated the Admin screen will load
3. Check GMail for a new mail item with subject "A new Survey report is available"
* This will contain a CSV file of all Survey data, for every brand

On the QA domain you will only be able to generate reports for Razorfish Surveys, due to a cock-up I've made (I look up the App ID and see if the word 'mcdonalds' is in it, to ensure McDonald's specific survey data is collected. The QA domain app id does not have the word 'mcdonalds' in it)



## Testing

The Google App Engine project has three available domains:

* Live: https://razorfish-client-survey.appspot.com
* Live: https://razorfish-mcdonalds-survey.appspot.com (McDonald's specific Clients)
* QA: https://razorfish-client-survey-qa.appspot.com

Change the app.yaml file in the root of this project to set the required environment. Remember you will need to manually add any Clients to the QA app to test their URLs

## Flush The Cache

1. Go to DOMAIN/admin/flush-cache

Restart the App using the App Engine Launcher. A deployment to the cloud based apps will automatically do this.

The cache will now be cleared - this is for any property file chnges to copy, for example


### Web Application

## Overview

# Models

This web app uses a set of defined GAE Models to create and store Surveys.

There is a base Survey Class, and then a branded Survey Class extends this:

* models/survey.py
** Survey
** RazorfishSurvey
** McDonaldsSurvey

# Views

All HTML is in the /templates/ directory
All CSS is in the /src/css/ directory, with brand specific style sheets to overwrite global styles
All JavaScript is in the /src/js/ directory
CSS and JavaScript is built to the /static/ directory using ANT tasks in /src/ant/ (just run the default ANT task to build the CSS and JS)
All copy content is in the form of property files in the /properties/ directory

# Controllers

Controllers are mixed between several Python classes in the root of the project:

* main.py
* handlers.py
* actions.py
* datastore.py
* utils.py
* configparsers.py
* mailservice.py








### TODO: Resolve GAE index.yaml Errors

To vacuum and rebuild your indexes:
1. Create a backup of your index.yaml specification.
2. Determine the indexes in state ERROR from your admin console: https://appengine.google.com/datastore/indexes?&app_id=s~razorfish-client-survey
3. Remove the definitions of the indexes in ERROR from your index.yaml file.
4. Run "appcfg.py vacuum_indexes your_app_dir/"
5. Wait until the ERROR indexes no longer appear in your admin console.
6. Replace the modified version of your index.yaml file with the original.
7. Run "appcfg.py update_indexes your_app_dir/"
