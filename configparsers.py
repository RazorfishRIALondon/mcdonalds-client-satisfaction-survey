from google.appengine.api import memcache
from ConfigParser import ConfigParser 

def loadPropertyFile(fileName):
	propertyFile = memcache.get(fileName+'.properties', namespace='global')
	if propertyFile is not None:
		return propertyFile
	else:
		propertyFilePath = 'properties/'+fileName+'.properties'
		propertyFile = ConfigParser()
		propertyFile.read(propertyFilePath)
		#memcache.set(fileName+'.properties', propertyFile, namespace='global')
		return propertyFile
