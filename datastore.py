import uuid
import datetime
import logging
from google.appengine.ext import db
from google.appengine.api.datastore_errors import BadValueError
from google.appengine.runtime.apiproxy_errors import CapabilityDisabledError
from google.appengine.api import memcache
import inspect

import actions
import utils
from models import survey as survey_model
from models import user as user_model

"""
Get Question results Count by Section & Question
"""
def get_results_by_count_section_question(sectionClass, question, date, requestedBrand):
	logging.debug('datastore.py : get_results_by_count_section_question() : sectionClass')
	query = sectionClass.gql('')
	
	results = list()
	for data in query:
		logging.debug(data)
		if hasattr(data, question):
			results.append(getattr(data, question))

	counted = utils.count_unsorted_list_items(results)
	percentage = utils.get_percentage(counted)

	return [counted,percentage]


def get_surveys_by_date_and_brand(date, brand):
	survey_class = utils.determine_survey(brand)
	#logging.debug(survey_class)
	# [ST]TODO: To get surveys by date, the operand should be >= meaning older than or up to the date specified, rather than younger than.
	# This is because the date select list in the UI states "since"
	query = survey_model.Survey.gql('WHERE created <= :created_date AND brand = :brand', created_date=date.isoformat(), brand=brand)
	return query

def get_surveys_by_brand(brand):
	#survey_class = utils.determine_survey(brand)
	#query = survey_class.gql('WHERE brand = :brand', brand=brand)
	logging.debug(brand)
	query = survey_model.Survey.all()

	# [ST] 2014: Filter only Surveys from the current year
	begnning_of_the_year = datetime.datetime.now()
	begnning_of_the_year = begnning_of_the_year.replace(day=1, month=1)
	query.filter('created >= ', begnning_of_the_year)

	# Filter the required Brand
	query.filter('brand = ', brand)
	return query

def set_all_arguments(user_id, brand, arguments):
	try:
	
		user = user_model.User.get_by_key_name(str(user_id))
		survey_class = utils.determine_survey(brand)
		logging.debug(survey_class)
		section_module = utils.determine_sections(brand)
		logging.debug(section_module)
		survey = survey_class(user_id=user, brand=brand)
		survey.put()
		# Iterate through each available Section Class
		for name, obj in inspect.getmembers(section_module):
			logging.debug(name)
			if inspect.isclass(obj):
				# Create an instance of the Section Class
				section = obj(parent=survey)		

				sectionProperties = section.properties()
				
				for propKey, propValue in sectionProperties.items():
					if propKey in arguments:
						try:
							# Set the property as the attribute and attribute value
							if propValue.data_type is int:
								setattr(section, propKey, int(arguments[propKey]))
							elif propValue.data_type is db.Text:
								encodedText = arguments[propKey].encode('ascii', 'xmlcharrefreplace')
								setattr(section, propKey, db.Text(encodedText))
						except Exception, e:
							logging.error(e)
							raise e

				# Add the Section to the datastore
				def txn_section():
					section.put()
				db.run_in_transaction(txn_section)
				
				# If the Survey instance has the Section...
				if hasattr(survey, str(section.kind()).lower()):
					# Add the Section to the Survey
					setattr(survey, str(section.kind()).lower(), section)
				
		# Update the Survey in the datastore
		survey.put()
	
			
	except Exception, e:
		logging.exception(e)
		raise e
	
def set_client(name, email, role, account, owner):
	# Add a client to the DB
	try:
		def txn():
			user_id = uuid.uuid4()
			logging.debug(user_id)
			client = user_model.User(key_name=str(user_id), user_id=str(user_id), name=name, email=email, role=role, account=account, owner=owner)
			client.put()
		db.run_in_transaction(txn)
	except Exception, e:
		logging.exception(e)
		raise e
	