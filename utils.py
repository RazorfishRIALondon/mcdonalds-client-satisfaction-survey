import os
from google.appengine.ext import db
from collections import defaultdict
from google.appengine.api import memcache
import logging
import cgi
import math
import inspect
import datetime
from operator import itemgetter
import cStringIO

"""
Import local scripts
"""
import configparsers
from models import survey as survey_model
from models import razorfish_sections
from models import mcdonalds_sections
from models import user as user_model
import datastore

def create_section_content(brand):
	try:
		surveyProps = configparsers.loadPropertyFile('survey_'+brand)  
		sections_module = determine_sections(brand)
		memcache_key = brand+'section:content'
		memcachedSectionContent = memcache.get(key=memcache_key, namespace='razorfish')
		
		if memcachedSectionContent is not None:
			logging.debug("Returning section:content from MEMCACHE")
			return memcachedSectionContent
		else:
			sections = list()
			for name, obj in inspect.getmembers(sections_module):
				if inspect.isclass(obj) and surveyProps.has_section(name):
					section = dict()
					section['name'] = surveyProps.get(name, 'name')
					section['instructions'] = surveyProps.get(name, 'instructions')
					section['id'] = name
					section['questions'] = obj.properties()
					section['index'] = surveyProps.get(name, 'index')
					section['error_message'] = surveyProps.get(name, 'error_message')
					sections.append(section)

			# Sort the Sections
			sections = sorted(sections, key=itemgetter('index'), reverse=False)
			
			# Save to memcache
			memcache.set(key=memcache_key, value=sections, namespace='razorfish')
			
			# return the content
			logging.debug("Returning section:content from method")

			return sections
		
	except ValueError, e:
		logging.error('create_section_content()')
		logging.error(e)
	
	
def get_percentage(items):
	count = 0
	percentages = dict()
	for key,value in items.items():
		count += value
	
	for key,value in items.items():
		a = float(value)
		b = float(count)
		
		result = (a/b)*100
		percentages[key] = result
	return percentages

def count_unsorted_list_items(items):
    """
    :param items: iterable of hashable items to count
    :type items: iterable

    :returns: dict of counts like Py2.7 Counter
    :rtype: dict
    """
    counts = defaultdict(int)
    for item in items:
        counts[int(item)] += 1
    return dict(counts)

def validate_brand(brand_requested):
	brandProps = configparsers.loadPropertyFile('brand')
	if brandProps.has_section(brand_requested):
		return True
	else:
		return False

def get_brands():
	brandProps = configparsers.loadPropertyFile('brand')	
	brands = list()
	sections = brandProps.sections()
	for section in sections:
		brand = dict()
		brand['id'] = section
		brand['name'] = brandProps.get(section, 'display_name')
		brands.append(brand)
	return brands
	
def validate_client_user(account, user_id):
	query = user_model.User.get_by_key_name(str(user_id))
	if query is not None:
		return True
	else:
		return False
	
"""
	[ST]TODO: Update theis method to handle the new PolyModel Class usage
	Currently getting properties of a Survey will not return all of the Survey Sections. 
	We will need to use db.query_descendants()
"""
def generate_survey_csv(brand):
	try:
		out=cStringIO.StringIO()
		surveyProps = configparsers.loadPropertyFile('survey_'+brand)
		
		"""
		Generate CSV file output VALUES
		"""
		surveys = datastore.get_surveys_by_brand(brand)

		# [ST] DEBUG: Returning just the first Survey, to test the Admin mail.send_to_admins() service 
		# due to quota size exception on the attachment
		# surveys = [surveys.get()]
		
		sortedSections = None
		counter = 0
		for survey in surveys:
			sections = dict()
			surveyResults = list()

			descendants = db.query_descendants(survey)
			for descendant in descendants:
				section = descendant
				_id = section.kind()
				# If the Section Model has properties, and it exists in the Survey Properties ConfigParser file (e.g. is not the User model)...
				if hasattr(section, 'properties') and surveyProps.has_section(_id):
					sections[_id] = dict()
					sections[_id]['index'] = surveyProps.getint(_id, 'index')
					sections[_id]['section'] = section
					surveyResults.append(sections[_id])

			sortedSections = sorted(surveyResults, key=itemgetter('index'), reverse=False)
			
			if counter == 0:
				# for each section within the survey
				out.write('Created,Client,Account,')
				for section in sortedSections:
					for question, data in section['section'].properties().items():
						out.write('\"'+str(surveyProps.get(section['section'].kind(), 'name')+' : '+data.verbose_name)+'\",')					
				out.write('\n')
			
			out.write(str(survey.created.date().isoformat())+',')
			out.write(str(survey.user_id.name)+',')
			out.write(str(survey.user_id.account)+',')
			for section in sortedSections:
				for question, data in section['section'].properties().items():
					answer = getattr(section['section'], question)
					# CGI Escape the output of the answer, in case there are '<', '>', '&' or '"' characters in there. The double quote will break the CSV formatting
					out.write('\"'+cgi.escape(str(answer), quote=True)+'\",')
			out.write('\n')
			counter += 1
			
		return out
	except (Exception), e:
		logging.error(e)
		raise e

def determine_survey(brand):
	survey = None
	if brand == 'razorfish':
		survey = survey_model.RazorfishSurvey
	elif brand == 'mcdonalds':
		survey = survey_model.McDonaldsSurvey
	return survey

def determine_sections(brand):
	sections = None
	if brand == 'razorfish':
		sections = razorfish_sections
	elif brand == 'mcdonalds':
		sections = mcdonalds_sections
	return sections

def get_home_content(brandProps):
	content = dict()
	for section in brandProps.sections():
		content[section] = dict()
		for key, value in brandProps.items(section):
			content[section][key] = value
	return content

def get_app_id():
	app_id = os.environ['APPLICATION_ID']
	if os.environ['SERVER_NAME'] == 'localhost':
		app_id = os.environ['SERVER_NAME']
	else:
		app_id = app_id.split('~')[1]
	return app_id