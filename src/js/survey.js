RIA.Survey = new Class({
	Implements:[Options],
	options:{
		token:null,
		brand:null
	},
	initialize: function(options) {
		this.setOptions(options);
		document.getElement("html").addClass("js")
		
		
		this._form = document.id("survey-form");
		this.shell = document.id("shell");
		this.survey = document.id("survey");
		this.sections = document.getElements(".section");
		this.startButton = document.id("start");
		this.currentSection = this.sections[0];
		this.navigationSections = document.getElements("#sections h2");

		this.navigationButtons = document.getElements("#progress a");
		this.previousButton = document.id("previous");
		this.nextButton = document.id("next");
		this.counter = document.id("counter");
		this.submitButton = document.id("submit");
		
		this.eventsArray = [];
		
		this.addEventListeners();

		if(this.currentSection && this.currentSection.get("data-index") == (this.sections.length-1)) {
			this.nextButton.addClass("hide");
			this.nextButton.removeClass("js-show");
			this.submitButton.removeClass("js-hide");
		}
	},
	addEventListeners: function() {
		if(this.navigationButtons) {
			this.navigationButtons.addEvents({
				"click":this.navigateSection.bind(this)
			});
		}

		if(this._form) {
			this._form.addEvents({
				"submit": this.formSubmission.bind(this)
			})
		}
		
	},
	formSubmission: function(e) {
		
		
		window.scrollTo(0, 0);
		
		var target = e.target,
		targetIndex = this.currentSection.get("data-index");
		this.valid = this.validateForm(this.currentSection);
		if(!this.valid) {
			if(e && e.preventDefault()) e.preventDefault();			
		}	


	},
	updateSectionHeader: function(currentSectionId) {
		this.navigationSections.each(function(header) {
			if(header.get("data-id") == currentSectionId) {
				header.addClass("active");
			} else {
				header.removeClass("active");
			}			
		},this);
	},
	updateNavigationProgress: function() {
		var sectionNumber = (this.currentSection.get("data-index").toInt()+1);
		this.counter.set("html", sectionNumber);
		
		if(this.currentSection.get("data-index") > 0) {
			this.previousButton.removeClass("hide");
		} else {
			this.previousButton.addClass("hide");
		}
		
		if(this.currentSection.get("data-index") == (this.sections.length-1)) {
			this.nextButton.addClass("hide");
			this.nextButton.removeClass("js-show");
			this.submitButton.removeClass("js-hide");
		} else {
			this.submitButton.addClass("js-hide");
			this.nextButton.removeClass("hide");
		}
	},
	navigateSection: function(e) {
		if(e && e.preventDefault()) e.preventDefault();
		
		window.scrollTo(0, 0);
		
		var target = e.target,
		targetIndex = this.currentSection.get("data-index");
		
		if(target.get("id") == "previous") {
			targetIndex--;			
		} else if(target.get("id") == "next") {
			targetIndex++;
			this.valid = this.validateForm(this.currentSection);
		}
		
		if(this.valid) {
			/**
			this.trackSection(this.currentSection);
			*/
			this.currentSection.addClass("js-hide");
			this.currentSection = this.sections[targetIndex];
			this.currentSection.removeClass("js-hide");
			this.updateSectionHeader(this.currentSection.get("id"));
			this.updateNavigationProgress();
		}
	},
	validateForm: function(targetSection) {
		targetSection.getElements("fieldset").removeClass("error");
		var anyErrors = false;
		targetSection.getElements("fieldset.question").each(function(fieldset) {
			var valid = false;
			fieldset.getElements("input[type=radio]").each(function(radio) {
				if(radio.checked) valid = true;
			},this);
			if(!valid) {
				anyErrors = true;
				/* 
				* Get the inner fieldset and add the error class to this
				*/
				fieldset.getElement("fieldset").addClass("error");
			}
		},this);

		if(anyErrors) {
			return false;
		} else {
			return true;
		}
	},
	trackSection: function(section) {
		/**
			Google Analytics Convention:
				_gaq.push(['_trackEvent', 'Category', 'Action', 'Label', 'Value', 'Non-interaction']);
				Category 		: Section ID : Question text [String]
				Action 			: 'Answer' [String]
				Label 			: Client UUID [String]
				Value 			: Answer value [Int]
				Non-interaction	: true/false
		*/
		if(_gaq) {
			
			section.getElements("fieldset.question").each(function(fieldset) {
				fieldset.getElements("input[type=radio]").each(function(radio) {
					if(radio.checked) {
						//Log.info(section.get("id")+" : "+fieldset.getElement("h3").get("text") +" : "+ 'Answer' +" : "+ this.options.token +" : "+ radio.get("value").toInt(), false)						
						//this.eventsArray.push(eArray);
						_gaq.push(["_trackEvent", (this.options.brand+" : "+section.get("id")+" : "+fieldset.getElement("h3").get("text")), 'Answer', this.options.token, radio.get("value").toInt(), false]);
					}					
				},this);
			},this);
			
			//Log.info(this.eventsArray);
			//var pushing = _gaq.push(this.eventsArray);
			//Log.info(pushing+" push commands failed");
		}
	}
});