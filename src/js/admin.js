RIA.Admin = new Class({
	Implements:[Options],
	options:{
		token:null,
		brand:null
	},
	initialize: function(options) {
		this.setOptions(options);
		document.getElement("html").addClass("js");
		
		this.sections = document.getElements(".section");
		this.startButton = document.id("start");
		this.navigationSections = document.getElements("#sections h2");

		this.addEventListeners();
	},
	addEventListeners: function() {
		if(this.navigationSections) {
			this.navigationSections.addEvents({
				"click":this.navigateSection.bind(this)
			});
		}
	},
	navigateSection: function(e) {
		if(e && e.preventDefault()) e.preventDefault();
		var target = e.target;
		
		/**
			Update the nav state
		*/
		this.navigationSections.each(function(nav) {
			if(nav.get("data-id") == target.get("data-id")) {
				nav.addClass("active");
			} else {
				nav.removeClass("active");
			}
		},this);
		
		/**
			Enable the selected Section
		*/
		this.sections.each(function(section) {
			if(section.get("id") == target.get("data-id")) {
				section.removeClass("js-hide");
			} else {
				section.addClass("js-hide");
			}
		},this);
	}
});