from google.appengine.api import memcache
import os
import logging
import datetime
import webapp2
import jinja2
from google.appengine.ext import webapp
from google.appengine.ext.webapp import template
from google.appengine.ext import db
from google.appengine.runtime.apiproxy_errors import CapabilityDisabledError   
from google.appengine.ext.db import BadValueError
from google.appengine.runtime import DeadlineExceededError
import inspect
from operator import itemgetter
from google.appengine.api import users
from google.appengine.api import app_identity
jinja_environment = jinja2.Environment(loader=jinja2.FileSystemLoader(os.path.dirname(__file__)+'/templates'))

"""
Import local scripts
"""
from models import user as user_model
import datastore
import actions
import configparsers
import utils
import mailservice

chartApiSubDomainList = [0,1,2,3,4,5,6,7,8,9]

class BaseHandler(webapp2.RequestHandler):
	def __init__(self, request=None, response=None):
		self.initialize(request, response)
		self.context = {}
		self.context['build_version'] = os.environ['CURRENT_VERSION_ID']
		self.context['today'] = datetime.datetime.today()
		self.context['default_version_hostname'] = app_identity.get_default_version_hostname()
	def render(self, template_name):
		template = jinja_environment.get_template(template_name+'.html')
		self.response.out.write(template.render(self.context))

class SurveyHandler(BaseHandler):
	def get(self, brand_requested, user_id_requested):
		global brand
		analyticsProps = configparsers.loadPropertyFile('analytics')
		gaAccount = analyticsProps.get('GA', 'ga_account')
		brandProps = configparsers.loadPropertyFile('brand')
		home_content = utils.get_home_content(brandProps)
		if brand_requested is not None and brand_requested != '' and user_id_requested is not None and user_id_requested != '' and brandProps.has_section(brand_requested):
			brand = brand_requested
			brandDisplayName = brandProps.get(brand, 'display_name')
			section_content = utils.create_section_content(brand)
			session_key = None
			
			if utils.validate_client_user(brand_requested, user_id_requested) and utils.validate_brand(brand_requested):
				viewState = 'new'
				self.context['home_content']=home_content
				self.context['sessionKey']=session_key
				self.context['sections']=section_content
				self.context['viewState']=viewState
				self.context['brand']=brand
				self.context['user']=user_id_requested
				self.context['brandDisplayName']=brandDisplayName
				self.context['ga_account']=gaAccount
				template = 'index'
				self.render(template)
			else:
				logging.warning('No valid Brand or User found')
				viewState = 'error-404'
				self.context['viewState']=viewState
				self.context['brand']=brand
				self.context['user']=user_id_requested
				self.context['brandDisplayName']=brandDisplayName
				self.context['ga_account']=gaAccount
				template = 'error'
				self.response.set_status(404)
				self.render(template)

		else:
			viewState = 'error-404'
			self.context['viewState']=viewState
			self.context['brand']='razorfish'
			self.context['brandDisplayName']='Razorfish'
			self.context['ga_account']=gaAccount
			template = 'error'
			self.response.set_status(404)
			self.render(template)
	def post(self, brand_requested, action):
		global brand
		analyticsProps = configparsers.loadPropertyFile('analytics')
		gaAccount = analyticsProps.get('GA', 'ga_account')
		if brand_requested is not None and brand_requested != '':
			
			user = self.request.POST.get('session')

			if utils.validate_brand(brand_requested):
				viewState = 'start'
				brand = brand_requested
				section_content = utils.create_section_content(brand)
				session_key = None
				app_version = os.environ['CURRENT_VERSION_ID']

				brandProps = configparsers.loadPropertyFile('brand')
				brandDisplayName = brandProps.get(brand, 'display_name')
			
				self.context['sessionKey']=session_key
				self.context['sections']=section_content
				self.context['viewState']=viewState
				self.context['brand']=brand
				self.context['user']=user
				self.context['brandDisplayName']=brandDisplayName
				self.context['ga_account']=gaAccount
				template = 'index'
				self.render(template)
			else:
				self.error(500)
		else:
			self.error(500)
class SurveySubmitHandler(BaseHandler):
	def get(self):
		# If no brand has been selected, default to Razorfish
		#self.redirect('/razorfish')
		analyticsProps = configparsers.loadPropertyFile('analytics')
		gaAccount = analyticsProps.get('GA', 'ga_account')
		viewState = 'error-404'
		self.context['viewState']=viewState
		self.context['brand']='razorfish'
		self.context['brandDisplayName']='Razorfish'
		self.context['ga_account']=gaAccount
		template = 'error'
		self.response.set_status(404)
		self.render(template)
	def post(self):
		surveyKey = None
		arguments = dict()
		analyticsProps = configparsers.loadPropertyFile('analytics')
		gaAccount = analyticsProps.get('GA', 'ga_account')
		brand_requested = self.request.POST.get('brand')
		user_id = self.request.POST.get('session')
		try:
			brandProps = configparsers.loadPropertyFile('brand')
			
			
			
			if brand_requested is not None and brand_requested != '':
				brand_requested = brand_requested

				
				for arg in self.request.arguments():
					arguments[str(arg)] = self.request.get(arg)

				surveyKey = datastore.set_all_arguments(user_id, brand_requested, arguments)
				
				self.redirect('/'+brand_requested+'/success')

		except (CapabilityDisabledError,BadValueError,ValueError), e:
			logging.error("SurveySubmitHandler() : Error saving data")
			logging.error(e)
			sectionContent = utils.create_section_content(brand_requested)
			# If a Survey has failed to be stored in the datastore we won't receive the surveyKey,
			#  so we need to send the Form arguments to the Mail service
			mailservice.announce_survey_error(arguments)
			# [ST]TODO: We need to have stored the User's answer data, to return the form to the current state
			userAnswers = dict()
			viewState = 'error-500'
			brandDisplayName = brandProps.get(brand_requested, 'display_name')
			self.context['viewState']=viewState
			self.context['brand']=brand_requested
			self.context['brandDisplayName']=brandDisplayName
			self.context['ga_account']=gaAccount
			template = 'error'
			self.response.set_status(500)
			self.render(template)

class AdminHandler(BaseHandler):
	def get(self):
		try:
			brandProps = configparsers.loadPropertyFile('brand')
			brand = 'razorfish'
			brandDisplayName = brandProps.get(brand, 'display_name')
			adminUser = users.get_current_user()
			brands = utils.get_brands()
			userData = dict()
			userData['email'] = adminUser.email()
			userData['nickname'] = adminUser.nickname()
			userData['id'] = adminUser.user_id()
			sections = None
			since = None
			sectionContent = utils.create_section_content(brand)
			date = None
			section = self.request.get("section")
			since = self.request.get("since")
			requestedBrand = self.request.get('brand')
			dateQuery = None
			today = datetime.date.today()

			
			
			if since is not "":
				if since == "yesterday":
					delta = datetime.timedelta(days=-1)
					dateQuery = today + delta
				elif since == "lastweek":
					delta = datetime.timedelta(days=-7)
					dateQuery = today + delta
				elif since == "lastmonth":
					delta = datetime.timedelta(days=-30)
					dateQuery = today + delta	
				#sections = actions.action_get_survey_by_date_and_brand(dateQuery, requestedBrand)
				sections = actions.action_get_survey_sections_by_brand(requestedBrand)
				
			self.context['adminUser']=userData
			self.context['brandDisplayName']=brandDisplayName
			self.context['brand']=brand
			self.context['brands']=brands
			self.context['requestedBrand']=requestedBrand
			self.context['requestedSection']=section
			self.context['sectionContent']=sectionContent
			self.context['chartApiSubDomainList']=chartApiSubDomainList
			self.context['sections']=sections
			self.context['since']=since
			template = 'admin'
			self.render(template)

		except Exception, e:
			logging.exception(e)
	def post(self):
		logging.debug('AdminHandler')
		self.error(500)

class CSVReportHandler(BaseHandler):
	def get(self):
		try:

			brandProps = configparsers.loadPropertyFile('brand')
			brand = 'razorfish'
			brandDisplayName = brandProps.get(brand, 'display_name')
			adminUser = users.get_current_user()
			userData = dict()
			userData['email'] = adminUser.email()
			requestedBrand = str(self.request.get('brand'))
			mailservice.report_all_surveys(requestedBrand, userData['email'])
			
			self.redirect('/admin')
		except (Exception), e:
			self.error(500)
		
class AddClientHandler(BaseHandler):
	def get(self):
		brandProps = configparsers.loadPropertyFile('brand')
		brand = 'razorfish'
		brandDisplayName = brandProps.get(brand, 'display_name')
		adminUser = users.get_current_user()
		userData = None
		
		if adminUser is not None:
			userData = dict()
			userData['email'] = adminUser.email()
			userData['nickname'] = adminUser.nickname()
			userData['id'] = adminUser.user_id()
		
		# Retrieve all Clients, with the most latest created listed first
		clients = user_model.User.gql("ORDER BY created, account, name DESC")
		
		client_survey_url_brand = 'razorfish'
		app_id = utils.get_app_id()
		if app_id.find('mcdonalds') != -1:
			client_survey_url_brand = 'mcdonalds'

		self.context['clients']=clients
		self.context['adminUser']=userData
		self.context['brandDisplayName']=brandDisplayName
		self.context['brand']=brand
		self.context['client_survey_url_brand']=client_survey_url_brand
		template = 'clients'
		self.render(template)

	def post(self):
		try:
			name = self.request.get('name')
			email = self.request.get('email')
			role = self.request.get('role')
			account = self.request.get('account')
			owner = self.request.get('owner')
		
			if name != '' and email != '' and role != '' and account !='' and owner != '':
				logging.debug('client info good')
				datastore.set_client(name, email, role, account, owner)
				self.redirect('/admin/clients')
			else:
				raise
		except Exception, e:
			logging.error('AddClientHandler : Error : No Client data entered')
			logging.error(e)
			self.context['brand']='razorfish'
			self.context['brandDisplayName']='Razorfish'
			self.context['error']='no-data'
			template = 'clients'
			self.response.set_status(500)
			self.render(template)

class SuccessHandler(BaseHandler):
	def get(self, brand_requested):
		analyticsProps = configparsers.loadPropertyFile('analytics')
		gaAccount = analyticsProps.get('GA', 'ga_account')
		global brand
		if brand_requested is not None and brand_requested != '':
			if '/' in brand_requested:
				requestSplit = brand_requested.split('/')
				brand_requested = requestSplit[0]
			else:
				brand_requested = brand_requested
			
			if utils.validate_brand(brand_requested):
				brand = brand_requested
				brandProps = configparsers.loadPropertyFile('brand')
				brandDisplayName = brandProps.get(brand, 'display_name')
		
				self.context['brandDisplayName']=brandDisplayName
				self.context['brand']=brand
				self.context['ga_account']=gaAccount
				template = 'success'
				self.render(template)
	def post(self):
		logging.debug('SuccessHandler')
		self.error(500);
		
class WildCardHandler(BaseHandler):
	def get(self, wildCard):
		logging.debug('WildCardHandler')
		analyticsProps = configparsers.loadPropertyFile('analytics')
		gaAccount = analyticsProps.get('GA', 'ga_account')
		viewState = 'error-404'
		self.context['viewState']=viewState
		self.context['brand']='razorfish'
		self.context['brandDisplayName']='Razorfish'
		self.context['ga_account']=gaAccount
		template = 'error'
		self.response.set_status(404)
		self.render(template)
	def post(self, wildCard):
		logging.debug('WildCardHandler')
		analyticsProps = configparsers.loadPropertyFile('analytics')
		gaAccount = analyticsProps.get('GA', 'ga_account')
		viewState = 'error-404'
		self.context['viewState']=viewState
		self.context['brand']='razorfish'
		self.context['brandDisplayName']='Razorfish'
		self.context['ga_account']=gaAccount
		template = 'error'
		self.response.set_status(404)
		self.render(template)

class FlushCacheController(BaseHandler):
	def get(self):
		try:
			key = str(self.request.get('key'))
			if key is not '':
				memcache.delete(key=key, namespace='Razorfish')
			else:
				memcache.flush_all()
			print 'Memcache flushed'
			return                 
		except Exception, e:
			logging.error(e)
			self.error(500)