import cgi
import logging
import datetime
from google.appengine.ext import db
from google.appengine.runtime.apiproxy_errors import CapabilityDisabledError
from google.appengine.ext.db import BadValueError
from google.appengine.api import memcache
import inspect
from operator import itemgetter

"""
Import local scripts
"""
from models import razorfish_sections
from models import mcdonalds_sections
from models import survey as survey_model
import configparsers
import datastore
import utils

def action_get_results_by_section(requestedSection, date, requestedBrand):
	try:
		brand = requestedBrand
		surveyProps = configparsers.loadPropertyFile('survey_'+brand)
		sectionContent = None
		dateFormat = None
		if date is not None and date is not "":
			dateFormat = date.isoformat()
		
		sections = memcache.get(key='admin:'+requestedBrand+':'+requestedSection+':count', namespace='razorfish')
		if sections is not None:
			logging.debug("Returning admin:"+requestedBrand+':'+requestedSection+":count from MEMCACHE")
		else:
			survey_class = utils.determine_survey(brand)
			section_module = utils.determine_sections(brand)
			surveys = list()
			if hasattr(section_module, requestedSection) and inspect.isclass(getattr(section_module, requestedSection)):
				
				sectionClass = getattr(section_module, requestedSection)	
				section = dict()
				section['id'] = sectionClass.kind().lower()
				section['name'] = sectionClass.kind()
				section['index'] = surveyProps.get(sectionClass.kind(), 'index')
				section['questions'] = list()
				
				for key, value in sectionClass.properties().items():
					question = dict()
					question['id'] = key
					question['name'] = value.verbose_name
					logging.debug('getting results for '+str(key))
					results = datastore.get_results_by_count_section_question(sectionClass, key, dateFormat, requestedBrand)
					logging.debug(results)
					question['results'] = results[0]
					question['percentages'] = results[1]
					question['options_length'] = len(value.choices)
					section['questions'].append(question)
				
				surveys.append(section)

				sections = sorted(surveys, key=itemgetter('index'), reverse=False)

				# Save to memcache
				#memcache.set(key='admin:'+requestedBrand+':'+requestedSection+':count', value=sections, time=86400, namespace='razorfish')
				
		return sections
	except (BadValueError,AttributeError,TypeError,ValueError,CapabilityDisabledError), e:
		logging.error('action_get_results_by_section()')
		logging.error(e)
		raise e	
	
"""
	ERROR WARNING! FLAWED: DO NOT USE
"""
def action_get_survey_by_date_and_brand(date, brand):
	
	memcacheKey = 'survey:results:'+str(date.isoformat())	
	memcacheResult = memcache.get(memcacheKey, namespace=brand)
	if memcacheResult is not None:
		logging.debug('Returning processed Survey results from MEMCACHE')
		return memcacheResult
	else:
		logging.debug('Returning processed Survey results from DATASTORE')
		surveys = datastore.get_surveys_by_date_and_brand(date, brand)	
		
		surveyProps = configparsers.loadPropertyFile('survey_'+brand)
	
		sections = dict()
		surveyResults = list()
		# Prepare an object with the Datastore data
		# for each survey

		"""
			[ST]TODO:
		 		Refactor all of this to use the db.query_descendants() method against the Survey model instance received
		 		This will catch pre-PolyModel Survey instances in the Datastore, and saves iterating over all Survey model instance
		 		properties looking for Survey Sections that are classes, and then getting their properties, etc, etc.

		 		Otherwise we end up collecting Survey model instances that have no connected Section model instances :(
		"""
		for survey in surveys:
			# for each section within the survey
			for surveyAttr in survey.properties():
				#logging.debug('actions.py : action_get_survey_by_date_and_brand() : surveyAttr')
				#logging.debug(surveyAttr)
				section = getattr(survey, surveyAttr)

				#logging.debug('actions.py : action_get_survey_by_date_and_brand() : section')
				#logging.debug(type(section))

				# [ST]TODO: Ugly patch for pre-PolyModel Surveys
				if type(section) is list and surveyAttr == '_class' and len(section) <= 1:
					logging.debug('We have a broken Razorfish Survey connection')
					child_sections = db.query_descendants(survey)
					for child in child_sections:
						logging.debug(child)
				else:
					logging.debug('We have found a Razorfish Survey')

				# If the Section Model has properties, and it exists in the Survey Properties ConfigParser file (e.g. is not the User model)...
				if section is not None and hasattr(section, 'properties') and surveyProps.has_section(section.kind()):
					# If we have not yet created a dict entry for this section, then do so
					
					if surveyAttr not in sections:
						sections[surveyAttr] = dict()
						sections[surveyAttr]['id'] = surveyAttr
						sections[surveyAttr]['created'] = survey.created
						sections[surveyAttr]['index'] = surveyProps.get(section.kind(), 'index')
						sections[surveyAttr]['name'] = surveyProps.get(section.kind(), 'name')
						sections[surveyAttr]['results'] = dict()
						surveyResults.append(sections[surveyAttr])

					# for question name and value in the section
					for question, prop in section.properties().items():
						if type(prop) == db.IntegerProperty:
							answer = getattr(section, question)
							# If we have not yet created a dict entry for this question in this section, then do so
							if question not in sections[surveyAttr]['results']:
								sections[surveyAttr]['results'][question] = dict()
								sections[surveyAttr]['results'][question]['id'] = question
								sections[surveyAttr]['results'][question]['name'] = prop.verbose_name
								sections[surveyAttr]['results'][question]['raw'] = list()
								sections[surveyAttr]['results'][question]['count'] = dict()
								sections[surveyAttr]['results'][question]['percentage'] = dict()
								sections[surveyAttr]['results'][question]['options_length'] = len(prop.choices)
								sections[surveyAttr]['results'][question]['type'] = 'multi'
							# Add the answer to the question list
							sections[surveyAttr]['results'][question]['raw'].append(answer)
						if type(prop) == db.TextProperty:
							answer = getattr(section, question)
							
							# If we have not yet created a dict entry for this question in this section, then do so
							if question not in sections[surveyAttr]['results']:
								sections[surveyAttr]['results'][question] = dict()
								sections[surveyAttr]['results'][question]['id'] = question
								sections[surveyAttr]['results'][question]['name'] = prop.verbose_name
								sections[surveyAttr]['results'][question]['answers'] = list()
								sections[surveyAttr]['results'][question]['type'] = 'single'
							
							# IF an answer exists, include this as a tuple with the User model object as the first argument and the answer Text value as the second argument
							if answer != '':	
								sections[surveyAttr]['results'][question]['answers'].append({'user':getattr(survey, 'user_id'), 'answer':answer, 'date':survey.created})
						
		for section, questionSet in sections.items():
			for question, data in questionSet['results'].items():
				# If we have a multiple choice question we need to process the results
				
				if data.has_key('raw') and type(data['raw']) is list:
					base = {}
					base[1]=0
					base[2]=0
					base[3]=0
					base[4]=0
					base[5]=0
					counted = utils.count_unsorted_list_items(data['raw'])				
					data['count'] = dict(base.items() + counted.items())
					sorted(data['count'], reverse=False)
					data['percentage'] = utils.get_percentage(data['count'])
		
		finalResult = sorted(surveyResults, key=itemgetter('index'), reverse=False)
		if len(finalResult) > 0:
			memcache.set(key=memcacheKey, value=finalResult, time=300, namespace=brand)
		return finalResult

"""
	@name: action_get_survey_sections_by_brand
	@description:
		Get all Survey Section results and process them, for the HTML Template Admin view
"""	
def action_get_survey_sections_by_brand(brand):
	try:
		memcache_key = 'survey:sections:'+str(brand)	
		memcache_result = memcache.get(memcache_key, namespace=brand)
		if memcache_result is not None:
			logging.debug('action_get_survey_sections_by_brand() : Returning processed Survey Section results from MEMCACHE')
			return memcache_result
		else:
			logging.debug('action_get_survey_sections_by_brand() : Returning processed Survey Section results from DATASTORE')
			# Get the Surveys from the Datastore by Brand
			surveys = datastore.get_surveys_by_brand(brand)	
			# Load the Survey Properties, for Content iteration in the HTML Template
			surveyProps = configparsers.loadPropertyFile('survey_'+brand)

			# Create an empty dict for Survey Sections
			sections = dict()
			# Create an empty list for all section results
			section_results = list()
			# For each Survey in the Datastore Query result
			for survey in surveys:
				# Get the Survey descendats
				survey_descendants = db.query_descendants(survey)
				
				for descendant in survey_descendants:
					# Set the Sections dict key to be the Kind (lowercase)
					_id = descendant.kind().lower()
					# Create a Section dict
					if _id not in sections:
						sections[_id] = dict(
							id=_id,
							created=survey.created,
							index=surveyProps.get(descendant.kind(), 'index'),
							name=surveyProps.get(descendant.kind(), 'name'),
							results = dict()
						)
						section_results.append(sections[_id])

					# for question name and value in the descendant section
					for question, prop in descendant.properties().items():
						# If the Property is of type Integer (Long returned from Datastore, actually)
						if type(prop) == db.IntegerProperty:
							answer = getattr(descendant, question)
							
							# If we have not yet created a dict entry for this question in this section, then do so
							if question not in sections[_id]['results']:
								sections[_id]['results'][question] = dict(
									id=question,
									name=prop.verbose_name,
									raw=list(),
									count=dict(),
									percentage=dict(),
									options_length=len(prop.choices),
									type='multi'
								)

							# Add the answer to the question list
							sections[_id]['results'][question]['raw'].append(answer)
							
						# If the Property is of type Text:
						if type(prop) == db.TextProperty:
							answer = getattr(descendant, question)
							
							# If we have not yet created a dict entry for this question in this section, then do so
							if question not in sections[_id]['results']:
								sections[_id]['results'][question] = dict(
									id=question,
									name=prop.verbose_name,
									answers=list(),
									type='single'
								)
							
							# If an answer exists, include this as a tuple with the User model object as the first argument and the answer Text value as the second argument
							if answer != '':	
								sections[_id]['results'][question]['answers'].append({
								'user':getattr(survey, 'user_id'),
								'answer':answer,
								'date':survey.created
							})

				
			# Once we have iterated through all resuts, we need further processing.
			# This calculates the percentages of answers for multiple choice questions
			for section, questionSet in sections.items():
				for question, data in questionSet['results'].items():
					# If we have a multiple choice question we need to process the results
					#logging.debug(question)
					#logging.debug(data)
					if data.has_key('raw') and type(data['raw']) is list:
						base = {}
						if brand == 'razorfish':
							base[0]=0
							base[1]=0
							base[2]=0
							base[3]=0
							base[4]=0
							base[5]=0
						elif brand == 'mcdonalds':
							base[0]=0
							base[1]=0
							base[2]=0
							base[3]=0
							base[4]=0
							
						counted = utils.count_unsorted_list_items(data['raw'])
						#logging.debug('counted')
						#logging.debug(counted)
						data['count'] = dict(base.items() + counted.items())
						sorted(data['count'], reverse=False)
						data['percentage'] = utils.get_percentage(data['count'])
			
			# Finally, we sort our Section Results list, using the index of the section as the sort key
			section_results = sorted(section_results, key=itemgetter('index'), reverse=False)
			
			# Add the result to Memcache, to expire in 5 minutes
			if len(section_results) > 0:
				memcache_stored = memcache.set(memcache_key, value=section_results, time=360, namespace=brand)
			return section_results
	except Exception, e:
		logging.error(type(e))
		logging.error(e)
		raise e