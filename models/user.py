#
#	Data Models
#
from google.appengine.ext import db
from google.appengine.api import users

	
class User(db.Model):
	created = db.DateTimeProperty(auto_now_add=True)
	user_id = db.StringProperty()
	name = db.StringProperty()
	email = db.StringProperty(indexed=False)
	role = db.StringProperty(indexed=False)
	account = db.CategoryProperty()
	owner = db.StringProperty()