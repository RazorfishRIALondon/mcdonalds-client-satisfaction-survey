#
#	Data Models
#
import datetime
from google.appengine.ext import db

"""
Import local scripts
"""
import configparsers

surveyPropsRazorfish = configparsers.loadPropertyFile('survey_razorfish')


class Strategy(db.Model):
	business = db.IntegerProperty(verbose_name=surveyPropsRazorfish.get('Strategy', 'business'), name='business', choices=set([0, 1, 2, 3, 4, 5]), indexed=False)
	customers = db.IntegerProperty(verbose_name=surveyPropsRazorfish.get('Strategy', 'customers'), name='customers', choices=set([0, 1, 2, 3, 4, 5]), indexed=False)
	insight = db.IntegerProperty(verbose_name=surveyPropsRazorfish.get('Strategy', 'insight'), name='insight', choices=set([0, 1, 2, 3, 4, 5]), indexed=False)
	digital_comms = db.IntegerProperty(verbose_name=surveyPropsRazorfish.get('Strategy', 'digital_comms'), name='digital_comms', choices=set([0, 1, 2, 3, 4, 5]), indexed=False)
	roi = db.IntegerProperty(verbose_name=surveyPropsRazorfish.get('Strategy', 'roi'), name='roi', choices=set([0, 1, 2, 3, 4, 5]), indexed=False)

class Creativity(db.Model):
	ideation = db.IntegerProperty(verbose_name=surveyPropsRazorfish.get('Creativity', 'ideation'), name='ideation', choices=set([0, 1, 2, 3, 4, 5]), indexed=False)
	experience = db.IntegerProperty(verbose_name=surveyPropsRazorfish.get('Creativity', 'experience'), name='experience', choices=set([0, 1, 2, 3, 4, 5]), indexed=False)
	partnership = db.IntegerProperty(verbose_name=surveyPropsRazorfish.get('Creativity', 'partnership'), name='partnership', choices=set([0, 1, 2, 3, 4, 5]), indexed=False)
	relevance = db.IntegerProperty(verbose_name=surveyPropsRazorfish.get('Creativity', 'relevance'), name='relevance', choices=set([0, 1, 2, 3, 4, 5]), indexed=False)
	execution = db.IntegerProperty(verbose_name=surveyPropsRazorfish.get('Creativity', 'execution'), name='execution', choices=set([0, 1, 2, 3, 4, 5]), indexed=False)
	integration = db.IntegerProperty(verbose_name=surveyPropsRazorfish.get('Creativity', 'integration'), name='integration', choices=set([0, 1, 2, 3, 4, 5]), indexed=False)
	
class Delivery(db.Model):
	integrity = db.IntegerProperty(verbose_name=surveyPropsRazorfish.get('Delivery', 'integrity'), name='integrity', choices=set([0, 1, 2, 3, 4, 5]), indexed=False)
	timeliness = db.IntegerProperty(verbose_name=surveyPropsRazorfish.get('Delivery', 'timeliness'), name='timeliness', choices=set([0, 1, 2, 3, 4, 5]), indexed=False)
	budgeting = db.IntegerProperty(verbose_name=surveyPropsRazorfish.get('Delivery', 'budgeting'), name='budgeting', choices=set([0, 1, 2, 3, 4, 5]), indexed=False)
	change_management = db.IntegerProperty(verbose_name=surveyPropsRazorfish.get('Delivery', 'change_management'), name='change_management', choices=set([0, 1, 2, 3, 4, 5]), indexed=False)
	quality = db.IntegerProperty(verbose_name=surveyPropsRazorfish.get('Delivery', 'quality'), name='quality', choices=set([0, 1, 2, 3, 4, 5]), indexed=False)
	
class Technical(db.Model):
	expertise = db.IntegerProperty(verbose_name=surveyPropsRazorfish.get('Technical', 'expertise'), name='expertise', choices=set([0, 1, 2, 3, 4, 5]), indexed=False)
	knowledge_gathering = db.IntegerProperty(verbose_name=surveyPropsRazorfish.get('Technical', 'knowledge_gathering'), name='knowledge_gathering', choices=set([0, 1, 2, 3, 4, 5]), indexed=False)
	thought_leadership = db.IntegerProperty(verbose_name=surveyPropsRazorfish.get('Technical', 'thought_leadership'), name='thought_leadership', choices=set([0, 1, 2, 3, 4, 5]), indexed=False)
	
class Relationship(db.Model):  
	importance = db.IntegerProperty(verbose_name=surveyPropsRazorfish.get('Relationship', 'importance'), name='importance', choices=set([0, 1, 2, 3, 4, 5]), indexed=False)
	openness = db.IntegerProperty(verbose_name=surveyPropsRazorfish.get('Relationship', 'openness'), name='openness', choices=set([0, 1, 2, 3, 4, 5]), indexed=False)
	announcements = db.IntegerProperty(verbose_name=surveyPropsRazorfish.get('Relationship', 'announcements'), name='announcements', choices=set([0, 1, 2, 3, 4, 5]), indexed=False)
	responses = db.IntegerProperty(verbose_name=surveyPropsRazorfish.get('Relationship', 'responses'), name='responses', choices=set([0, 1, 2, 3, 4, 5]), indexed=False)
	proactivity = db.IntegerProperty(verbose_name=surveyPropsRazorfish.get('Relationship', 'proactivity'), name='proactivity', choices=set([0, 1, 2, 3, 4, 5]), indexed=False)
	
class Overall(db.Model):
	enjoyment = db.IntegerProperty(verbose_name=surveyPropsRazorfish.get('Overall', 'enjoyment'), name='enjoyment', choices=set([0, 1, 2, 3, 4, 5]), indexed=False)
	stimulation = db.IntegerProperty(verbose_name=surveyPropsRazorfish.get('Overall', 'stimulation'), name='stimulation',  choices=set([0, 1, 2, 3, 4, 5]), indexed=False)
	empathy = db.IntegerProperty(verbose_name=surveyPropsRazorfish.get('Overall', 'empathy'), name='empathy', choices=set([0, 1, 2, 3, 4, 5]), indexed=False)
	recommend = db.IntegerProperty(verbose_name=surveyPropsRazorfish.get('Overall', 'recommend'), name='recommend', choices=set([0, 1, 2, 3, 4, 5]), indexed=False)
	ranking = db.IntegerProperty(verbose_name=surveyPropsRazorfish.get('Overall', 'ranking'), name='ranking', choices=set([0, 1, 2, 3, 4, 5]), indexed=False)
	
class OpenQuestions(db.Model):
	more = db.TextProperty(verbose_name=surveyPropsRazorfish.get('OpenQuestions', 'more'), name='more', indexed=False)
	less = db.TextProperty(verbose_name=surveyPropsRazorfish.get('OpenQuestions', 'less'), name='less', indexed=False)
	new = db.TextProperty(verbose_name=surveyPropsRazorfish.get('OpenQuestions', 'new'), name='new', indexed=False)