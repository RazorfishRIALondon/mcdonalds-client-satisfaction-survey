#
#	Data Models
#
import datetime
from google.appengine.ext import db
from google.appengine.ext.db import polymodel
from google.appengine.api import users
from models import user as user_model

from models import razorfish_sections
from models import mcdonalds_sections

class Survey(polymodel.PolyModel):
	created = db.DateTimeProperty(auto_now_add=True)
	brand = db.CategoryProperty()
	user_id = db.ReferenceProperty(user_model.User, collection_name='user')
	
class RazorfishSurvey(Survey):
	strategy = db.ReferenceProperty(razorfish_sections.Strategy, collection_name='strategy')
	creativity = db.ReferenceProperty(razorfish_sections.Creativity, collection_name='creativity')
	delivery = db.ReferenceProperty(razorfish_sections.Delivery, collection_name='delivery')
	technical = db.ReferenceProperty(razorfish_sections.Technical, collection_name='technical')
	relationship = db.ReferenceProperty(razorfish_sections.Relationship, collection_name='relationship')
	overall = db.ReferenceProperty(razorfish_sections.Overall, collection_name='overall')
	openquestions = db.ReferenceProperty(razorfish_sections.OpenQuestions, collection_name='openquestions')


class McDonaldsSurvey(Survey):
	mstrategy = db.ReferenceProperty(mcdonalds_sections.MStrategy, collection_name='mcd_strategy')
	mcreativity = db.ReferenceProperty(mcdonalds_sections.MCreativity, collection_name='mcd_creativity')
	mdelivery = db.ReferenceProperty(mcdonalds_sections.MDelivery, collection_name='mcd_delivery')
	mrelationship = db.ReferenceProperty(mcdonalds_sections.MRelationship, collection_name='mcd_relationship')
	moverall = db.ReferenceProperty(mcdonalds_sections.MOverall, collection_name='mcd_overall')
	mopenquestions = db.ReferenceProperty(mcdonalds_sections.MOpenQuestions, collection_name='mcd_openquestions')
