#
#	Data Models
#
import logging
import datetime
from google.appengine.ext import db
from google.appengine.api import users

"""
Import local scripts
"""
import configparsers

surveyProps = configparsers.loadPropertyFile('survey_mcdonalds')

class MStrategy(db.Model):
	business = db.IntegerProperty(verbose_name=surveyProps.get('MStrategy', 'business'), name='business', choices=set([0, 1, 2, 3, 4]), indexed=False)
	proactive = db.IntegerProperty(verbose_name=surveyProps.get('MStrategy', 'proactive'), name='proactive', choices=set([0, 1, 2, 3, 4]), indexed=False)
	training = db.IntegerProperty(verbose_name=surveyProps.get('MStrategy', 'training'), name='training', choices=set([0, 1, 2, 3, 4]), indexed=False)
	analysis = db.IntegerProperty(verbose_name=surveyProps.get('MStrategy', 'analysis'), name='analysis', choices=set([0, 1, 2, 3, 4]), indexed=False)
	roi = db.IntegerProperty(verbose_name=surveyProps.get('MStrategy', 'roi'), name='roi', choices=set([0, 1, 2, 3, 4]), indexed=False)
	
class MCreativity(db.Model):
	experience = db.IntegerProperty(verbose_name=surveyProps.get('MCreativity', 'experience'), name='experience', choices=set([0, 1, 2, 3, 4]), indexed=False)
	partnership = db.IntegerProperty(verbose_name=surveyProps.get('MCreativity', 'partnership'), name='partnership', choices=set([0, 1, 2, 3, 4]), indexed=False)
	innovation = db.IntegerProperty(verbose_name=surveyProps.get('MCreativity', 'innovation'), name='innovation', choices=set([0, 1, 2, 3, 4]), indexed=False)
	execution = db.IntegerProperty(verbose_name=surveyProps.get('MCreativity', 'execution'), name='execution', choices=set([0, 1, 2, 3, 4]), indexed=False)
	
class MDelivery(db.Model):
	change_management = db.IntegerProperty(verbose_name=surveyProps.get('MDelivery', 'change_management'), name='change_management', choices=set([0, 1, 2, 3, 4]), indexed=False)
	budgeting = db.IntegerProperty(verbose_name=surveyProps.get('MDelivery', 'budgeting'), name='budgeting', choices=set([0, 1, 2, 3, 4]), indexed=False)
	timeliness = db.IntegerProperty(verbose_name=surveyProps.get('MDelivery', 'timeliness'), name='timeliness', choices=set([0, 1, 2, 3, 4]), indexed=False)
	integrity = db.IntegerProperty(verbose_name=surveyProps.get('MDelivery', 'integrity'), name='integrity', choices=set([0, 1, 2, 3, 4]), indexed=False)

class MRelationship(db.Model):  
	importance = db.IntegerProperty(verbose_name=surveyProps.get('MRelationship', 'importance'), name='importance', choices=set([0, 1, 2, 3, 4]), indexed=False)
	responses = db.IntegerProperty(verbose_name=surveyProps.get('MRelationship', 'responses'), name='responses', choices=set([0, 1, 2, 3, 4]), indexed=False)	
	support = db.IntegerProperty(verbose_name=surveyProps.get('MRelationship', 'support'), name='support', choices=set([0, 1, 2, 3, 4]), indexed=False)
	agencies = db.IntegerProperty(verbose_name=surveyProps.get('MRelationship', 'agencies'), name='agencies', choices=set([0, 1, 2, 3, 4]), indexed=False)
	
class MOverall(db.Model):
	enjoyment = db.IntegerProperty(verbose_name=surveyProps.get('MOverall', 'enjoyment'), name='enjoyment', choices=set([0, 1, 2, 3, 4]), indexed=False)
	recommend = db.IntegerProperty(verbose_name=surveyProps.get('MOverall', 'recommend'), name='recommend', choices=set([0, 1, 2, 3, 4]), indexed=False)
	stimulation = db.IntegerProperty(verbose_name=surveyProps.get('MOverall', 'stimulation'), name='stimulation',  choices=set([0, 1, 2, 3, 4]), indexed=False)
	
class MOpenQuestions(db.Model):
	more = db.TextProperty(verbose_name=surveyProps.get('MOpenQuestions', 'more'), name='more', indexed=False)
	less = db.TextProperty(verbose_name=surveyProps.get('MOpenQuestions', 'less'), name='less', indexed=False)