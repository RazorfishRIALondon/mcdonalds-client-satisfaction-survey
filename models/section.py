#
#	Data Models
#
import datetime
from google.appengine.ext import db
from google.appengine.ext.db import polymodel
from google.appengine.api import users

"""
Import local scripts
"""
import configparsers

surveyProps = configparsers.loadPropertyFile('survey')


class Strategy(db.Model):
	business = db.IntegerProperty(verbose_name=surveyProps.get('Strategy', 'business'), name='business', choices=set([0, 1, 2, 3, 4, 5]), indexed=False)
	customers = db.IntegerProperty(verbose_name=surveyProps.get('Strategy', 'customers'), name='customers', choices=set([0, 1, 2, 3, 4, 5]), indexed=False)
	insight = db.IntegerProperty(verbose_name=surveyProps.get('Strategy', 'insight'), name='insight', choices=set([0, 1, 2, 3, 4, 5]), indexed=False)
	digital_comms = db.IntegerProperty(verbose_name=surveyProps.get('Strategy', 'digital_comms'), name='digital_comms', choices=set([0, 1, 2, 3, 4, 5]), indexed=False)
	roi = db.IntegerProperty(verbose_name=surveyProps.get('Strategy', 'roi'), name='roi', choices=set([0, 1, 2, 3, 4, 5]), indexed=False)

class Creativity(db.Model):
	ideation = db.IntegerProperty(verbose_name=surveyProps.get('Creativity', 'ideation'), name='ideation', choices=set([0, 1, 2, 3, 4, 5]), indexed=False)
	experience = db.IntegerProperty(verbose_name=surveyProps.get('Creativity', 'experience'), name='experience', choices=set([0, 1, 2, 3, 4, 5]), indexed=False)
	partnership = db.IntegerProperty(verbose_name=surveyProps.get('Creativity', 'partnership'), name='partnership', choices=set([0, 1, 2, 3, 4, 5]), indexed=False)
	relevance = db.IntegerProperty(verbose_name=surveyProps.get('Creativity', 'relevance'), name='relevance', choices=set([0, 1, 2, 3, 4, 5]), indexed=False)
	execution = db.IntegerProperty(verbose_name=surveyProps.get('Creativity', 'execution'), name='execution', choices=set([0, 1, 2, 3, 4, 5]), indexed=False)
	integration = db.IntegerProperty(verbose_name=surveyProps.get('Creativity', 'integration'), name='integration', choices=set([0, 1, 2, 3, 4, 5]), indexed=False)
	
class Delivery(db.Model):
	integrity = db.IntegerProperty(verbose_name=surveyProps.get('Delivery', 'integrity'), name='integrity', choices=set([0, 1, 2, 3, 4, 5]), indexed=False)
	timeliness = db.IntegerProperty(verbose_name=surveyProps.get('Delivery', 'timeliness'), name='timeliness', choices=set([0, 1, 2, 3, 4, 5]), indexed=False)
	budgeting = db.IntegerProperty(verbose_name=surveyProps.get('Delivery', 'budgeting'), name='budgeting', choices=set([0, 1, 2, 3, 4, 5]), indexed=False)
	change_management = db.IntegerProperty(verbose_name=surveyProps.get('Delivery', 'change_management'), name='change_management', choices=set([0, 1, 2, 3, 4, 5]), indexed=False)
	quality = db.IntegerProperty(verbose_name=surveyProps.get('Delivery', 'quality'), name='quality', choices=set([0, 1, 2, 3, 4, 5]), indexed=False)
	
class Technical(db.Model):
	expertise = db.IntegerProperty(verbose_name=surveyProps.get('Technical', 'expertise'), name='expertise', choices=set([0, 1, 2, 3, 4, 5]), indexed=False)
	knowledge_gathering = db.IntegerProperty(verbose_name=surveyProps.get('Technical', 'knowledge_gathering'), name='knowledge_gathering', choices=set([0, 1, 2, 3, 4, 5]), indexed=False)
	thought_leadership = db.IntegerProperty(verbose_name=surveyProps.get('Technical', 'thought_leadership'), name='thought_leadership', choices=set([0, 1, 2, 3, 4, 5]), indexed=False)
	
class Relationship(db.Model):  
	importance = db.IntegerProperty(verbose_name=surveyProps.get('Relationship', 'importance'), name='importance', choices=set([0, 1, 2, 3, 4, 5]), indexed=False)
	openness = db.IntegerProperty(verbose_name=surveyProps.get('Relationship', 'openness'), name='openness', choices=set([0, 1, 2, 3, 4, 5]), indexed=False)
	announcements = db.IntegerProperty(verbose_name=surveyProps.get('Relationship', 'announcements'), name='announcements', choices=set([0, 1, 2, 3, 4, 5]), indexed=False)
	responses = db.IntegerProperty(verbose_name=surveyProps.get('Relationship', 'responses'), name='responses', choices=set([0, 1, 2, 3, 4, 5]), indexed=False)
	proactivity = db.IntegerProperty(verbose_name=surveyProps.get('Relationship', 'proactivity'), name='proactivity', choices=set([0, 1, 2, 3, 4, 5]), indexed=False)
	
class Overall(db.Model):
	enjoyment = db.IntegerProperty(verbose_name=surveyProps.get('Overall', 'enjoyment'), name='enjoyment', choices=set([0, 1, 2, 3, 4, 5]), indexed=False)
	stimulation = db.IntegerProperty(verbose_name=surveyProps.get('Overall', 'stimulation'), name='stimulation',  choices=set([0, 1, 2, 3, 4, 5]), indexed=False)
	empathy = db.IntegerProperty(verbose_name=surveyProps.get('Overall', 'empathy'), name='empathy', choices=set([0, 1, 2, 3, 4, 5]), indexed=False)
	recommend = db.IntegerProperty(verbose_name=surveyProps.get('Overall', 'recommend'), name='recommend', choices=set([0, 1, 2, 3, 4, 5]), indexed=False)
	ranking = db.IntegerProperty(verbose_name=surveyProps.get('Overall', 'ranking'), name='ranking', choices=set([0, 1, 2, 3, 4, 5]), indexed=False)
	
class OpenQuestions(db.Model):
	more = db.TextProperty(verbose_name=surveyProps.get('OpenQuestions', 'more'), name='more', indexed=False)
	less = db.TextProperty(verbose_name=surveyProps.get('OpenQuestions', 'less'), name='less', indexed=False)
	new = db.TextProperty(verbose_name=surveyProps.get('OpenQuestions', 'new'), name='new', indexed=False)