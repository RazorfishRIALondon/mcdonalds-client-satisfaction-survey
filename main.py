#!/usr/bin/env python
import re
import os
import codecs
import logging
import urllib
import urllib2
import datetime
import webapp2

from google.appengine.api import urlfetch
from google.appengine.api import quota
from google.appengine.ext import webapp
from google.appengine.ext.webapp import template
from google.appengine.ext.webapp.util import run_wsgi_app
from google.appengine.ext import db
from google.appengine.runtime.apiproxy_errors import CapabilityDisabledError
from google.appengine.api import memcache
from google.appengine.api import taskqueue
from google.appengine.ext import deferred

"""
Import local scripts
"""
import handlers

requestAdmin = "/admin"
requestAddClient = "/admin/clients"
requestAdminReportCSV = "/admin/report"
requestSuccess = r"/(.*)/success"
requestSurveySubmit = "/"
requestSurveyStart = r"/(.*)/(.*)"
requestSurveyIntro = r"/(.*)/(.*)"
requestWildcard = r"(.*)"
requestClearMemcache = '/admin/flush-cache'

# Razorfish is default Brand
global brand
brand = 'razorfish'




app = webapp2.WSGIApplication([
	(requestSurveySubmit, handlers.SurveySubmitHandler),
	(requestClearMemcache, handlers.FlushCacheController),
	(requestAdmin, handlers.AdminHandler),
	(requestAdminReportCSV, handlers.CSVReportHandler),
	(requestAddClient, handlers.AddClientHandler),
	(requestSuccess, handlers.SuccessHandler),
	(requestSurveyIntro, handlers.SurveyHandler),
	(requestSurveyStart, handlers.SurveyHandler),
	(requestWildcard, handlers.WildCardHandler)	
	],debug=True)
