from google.appengine.runtime import apiproxy_errors
from google.appengine.api import mail
from models import survey as survey_model
import logging
import datetime
import configparsers
import inspect
import os
# Import local scripts
import utils

def announce_new_survey(surveyKey):
	try:
		mailProps = configparsers.loadPropertyFile('mail')
		survey = survey_model.Survey.get(surveyKey)
		message = mail.EmailMessage()
		message.sender = mailProps.get('NewSurvey', 'sender')
		message.to = mailProps.get('NewSurvey', 'to')
		message.subject = mailProps.get('NewSurvey', 'subject_success')
		message.body="""
		A new Survey has been submitted:

		Survey Created: """+str(survey.created.isoformat())+"""
		Survey ID: """+str(survey.key())
		message.send()
	except (Exception, apiproxy_errors.ApplicationError, apiproxy_errors.OverQuotaError), e:
		logging.error(e)
		logging.error('announce_new_survey() : mail could not be sent as the 24-hour quota has been exceeded')
		
def announce_survey_error(arguments):
	try:
		today = datetime.date.today()
		mailProps = configparsers.loadPropertyFile('mail')
		message = mail.EmailMessage()
		message.sender = mailProps.get('NewSurvey', 'sender')
		message.to = mailProps.get('NewSurvey', 'to')
		message.subject = mailProps.get('NewSurvey', 'subject_datastore_failure')
		message.body="""A Survey failed to save: \n\nSurvey Created: """+str(today)+"""\n\nSurvey answers: """+str(arguments)+"""\n\nend message"""
		message.send()
	except (Exception, apiproxy_errors.ApplicationError, apiproxy_errors.OverQuotaError), e:
		logging.error(e)
		logging.error('announce_new_survey() : mail could not be sent as the 24-hour quota has been exceeded')
		
def report_all_surveys(requestedBrand, emailRecipient):
	try:
		today = datetime.date.today()
		ioCsvOuput = utils.generate_survey_csv(requestedBrand)
		"""
		# [ST] Pre Py2.7 Upgrade
		mailProps = configparsers.loadPropertyFile('mail')
		message = mail.EmailMessage()
		message.sender = mailProps.get('NewSurvey', 'sender')
		message.to = emailRecipient
		message.subject = mailProps.get('NewSurvey', 'subject_report')
		message.body='New report'
		message.attachments=[(str(os.environ['HTTP_HOST'])+'-'+str(today.isoformat())+'.csv', ioCsvOuput.getvalue())]
		message.send()
		"""
		# [ST] Post Py2.7 Upgrade
		mailProps = configparsers.loadPropertyFile('mail')
		mail.send_mail_to_admins(
			sender=mailProps.get('NewSurvey', 'sender'),
			subject=mailProps.get('NewSurvey', 'subject_report'),
			body='New report',
			**dict(
				attachments=[(str(os.environ['HTTP_HOST'])+'-'+str(today.isoformat())+'.csv', ioCsvOuput.getvalue())]
			)
		)
	except (Exception, apiproxy_errors.ApplicationError, apiproxy_errors.OverQuotaError), e:
		logging.error(e)
		logging.error('report_all_surveys() : mail could not be sent as the 24-hour quota has been exceeded')
		raise e